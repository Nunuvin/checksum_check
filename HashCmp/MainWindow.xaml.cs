﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;

namespace HashCmp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCompare_Click(object sender, RoutedEventArgs e)
        {
            String encryptType;
            String filepath;
            String hash;
            encryptType = EncryptionType.SelectionBoxItem.ToString();
            filepath = FilePath.Text;
            hash = ExpectedHash.Text.ToUpper();
            Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
            switch (encryptType)
            {
                case "MD5":
                    //label.Content = "MD5";
                    try
                    {
                        using (var md5 = MD5.Create())
                        {
                            using (var stream = File.OpenRead(filepath))
                            {
                                String res = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "");
                                Result.Text = res;
                                //if (hash.Equals((String)BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", ""))) //this produces a compeletly different strng
                                if (hash.Equals(res))
                                {
                                    //label.Content = "YAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                                }
                                else
                                {
                                    //label.Content = "NAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                        Result.Text = "Exception Occured";
                    }
                    break;
                case "SHA1":
                    try
                    {
                        using (var sha1 = SHA1.Create())
                        {
                            using (var stream = File.OpenRead(filepath))
                            {
                                String res = BitConverter.ToString(sha1.ComputeHash(stream)).Replace("-", "");
                                Result.Text = res;
                                //if (hash.Equals((String)BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", ""))) //this produces a compeletly different strng
                                if (hash.Equals(res))
                                {
                                    //label.Content = "YAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                                }
                                else
                                {
                                    //label.Content = "NAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                        Result.Text = "Exception Occured";
                    }
                    break;
                case "SHA256":
                    try
                    {
                        using (var sha256 = SHA256.Create())
                        {
                            using (var stream = File.OpenRead(filepath))
                            {
                                String res = BitConverter.ToString(sha256.ComputeHash(stream)).Replace("-", "");
                                Result.Text = res;
                                //if (hash.Equals((String)BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", ""))) //this produces a compeletly different strng
                                if (hash.Equals(res))
                                {
                                    //label.Content = "YAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                                }
                                else
                                {
                                    //label.Content = "NAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                        Result.Text = "Exception Occured";
                    }
                    break;
                case "SHA512":
                    try
                    {
                        using (var sha512 = SHA512.Create())
                        {
                            using (var stream = File.OpenRead(filepath))
                            {
                                String res = BitConverter.ToString(sha512.ComputeHash(stream)).Replace("-", "");
                                Result.Text = res;
                                //if (hash.Equals((String)BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", ""))) //this produces a compeletly different strng
                                if (hash.Equals(res))
                                {
                                    //label.Content = "YAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, true);
                                }
                                else
                                {
                                    //label.Content = "NAY";
                                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                        Result.Text = "Exception Occured";
                    }
                    break;
                default:
                    Matching.SetCurrentValue(CheckBox.IsCheckedProperty, false);
                    break;
            }
           

        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "File"; // Default file name
            //dlg.DefaultExt = ".txt"; // Default file extension
            //dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                FilePath.Text = filename;

            }
           
        }

    
    }
}
